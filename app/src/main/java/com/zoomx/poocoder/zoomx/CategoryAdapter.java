package com.zoomx.poocoder.zoomx;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by poocoder on 11/14/17.
 */

public class CategoryAdapter extends ArrayAdapter<CategoryItem> {
    public ArrayList<CategoryItem> itemSet;
    Context mContext;

    FirebaseAuth mAuth;
    FirebaseDatabase mFirebaseDataBase;
    DatabaseReference mDataRef;

    static class ViewHolder {
        TextView categoryName;
        TextView leftEye, rightEye;
        ImageView categoryDelete;
    }

    public CategoryAdapter(@NonNull Context context, ArrayList<CategoryItem> objects) {
        super(context, R.layout.category_item, objects);
        itemSet = objects;
        mContext = context;
    }


    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final CategoryItem categoryItem = getItem(position);

        ViewHolder viewHolder;

        final View result;

        mAuth = FirebaseAuth.getInstance();

        if(convertView == null){
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.category_item, parent, false);

            viewHolder.categoryName = convertView.findViewById(R.id.category_name);
            viewHolder.leftEye = convertView.findViewById(R.id.item_left_eye);
            viewHolder.rightEye = convertView.findViewById(R.id.item_right_eye);
            viewHolder.categoryDelete = convertView.findViewById(R.id.category_delete);

            result = convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }

        viewHolder.categoryName.setText(categoryItem.name);
        viewHolder.leftEye.setText(categoryItem.getLeftEye());
        viewHolder.rightEye.setText(categoryItem.getRightEye());
        viewHolder.categoryDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Remove Item From DB

                if(mAuth.getCurrentUser() != null) {
                    FirebaseDatabase.getInstance().getReference().child(mAuth.getCurrentUser().getUid()).child("category")
                            .child(categoryItem.getName()).removeValue();

                    Log.d("DELETE", mAuth.getCurrentUser().getUid() + "");


                    itemSet.remove(position);
                    notifyDataSetChanged();
                }
               // Toast.makeText(getContext(), "Hh", Toast.LENGTH_SHORT).show();


            }
        });
        viewHolder.categoryDelete.setTag(position);


        // Return the completed view to render on screen
        return convertView;
    }
}
