package com.zoomx.poocoder.zoomx;

/**
 * Created by poocoder on 11/24/17.
 */


public class User {
    private String nameS;
    private String emailS;

    public String getPassS() {
        return passS;
    }

    public void setPassS(String passS) {
        this.passS = passS;
    }

    private String passS;




    public User(String nameS, String emailS, String passS){
        this.nameS = nameS;
        this.emailS = emailS;
        this.passS = passS;

    }

    public String getNameS() {
        return nameS;
    }

    public void setNameS(String nameS) {
        this.nameS = nameS;
    }

    public String getEmailS() {
        return emailS;
    }

    public void setEmailS(String emailS) {
        this.emailS = emailS;
    }
}
