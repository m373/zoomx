package com.zoomx.poocoder.zoomx;

import android.Manifest;
import android.app.ActionBar;
import android.app.DialogFragment;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.AsyncLayoutInflater;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.DragEvent;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.SlidingDrawer;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.jar.Attributes;

public class MainActivity extends AppCompatActivity implements View.OnClickListener,
        CategoryDialog.OnCategoryNameListener, NavigationView.OnNavigationItemSelectedListener{

    DialogFragment mBluetoothDialog;
    DialogFragment mCategoryDialog;
    LinearLayout mRelativeLayout = null;
    final static int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 100;
    final static String TAG = "DATABASE";
    NavigationView navigationView;


    Toolbar toolbar;
    ArrayList<CategoryItem> categoryItems;
    CategoryAdapter categoryAdapter;
    ListView categoryList;
    FloatingActionButton leftAdd, rightAdd, leftMinus, rightMinus;
    FloatingActionButton addItem;
    TextView leftEye, rightEye, nameT, emailT;

    String nameS = "", emailS = "";

    MenuItem login, signup;

    private FirebaseAuth mAuth;
    public FirebaseAuth.AuthStateListener mAuthListener;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;

    public static BluetoothDevice device;
    public static Handler handler;
    public static List<BluetoothGattService> services;
    public static BluetoothGatt currentGatt;
    private BluetoothGattCharacteristic characteristic;

    private boolean loggedin = false;


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Enable offline usage
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        setContentView(R.layout.activity_main);

        addItem = findViewById(R.id.add_category_btn);
        categoryList = findViewById(R.id.category_list);
        toolbar = findViewById(R.id.my_toolbar);
        mRelativeLayout = findViewById(R.id.main_layout);
        leftEye = findViewById(R.id.left_eye);
        rightEye = findViewById(R.id.right_eye);
        leftAdd = findViewById(R.id.left_plus_btn);
        leftMinus = findViewById(R.id.left_minus_btn);
        rightAdd = findViewById(R.id.right_plus_btn);
        rightMinus = findViewById(R.id.right_minus_btn);
        login = findViewById(R.id.nav_login);
        signup = findViewById(R.id.nav_sign_up);

        mAuth = FirebaseAuth.getInstance();



        categoryItems = new ArrayList<>();
        categoryAdapter = new CategoryAdapter(this, categoryItems);
        categoryList.setAdapter(categoryAdapter);
        addItem.setOnClickListener(this);


        leftAdd.setOnClickListener(this);
        rightAdd.setOnClickListener(this);
        leftMinus.setOnClickListener(this);
        rightMinus.setOnClickListener(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
                ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);

        toggle.syncState();

        navigationView = findViewById(R.id.nav_view);
                navigationView.setNavigationItemSelectedListener(this);




        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_CONTACTS)) {
            } else {

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.BLUETOOTH_ADMIN, Manifest.permission.ACCESS_COARSE_LOCATION},
                        MY_PERMISSIONS_REQUEST_READ_CONTACTS);
            }

        }

        firebaseAuthSync();


        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {

                FirebaseUser user = firebaseAuth.getCurrentUser();

                if(user == null){
                    dataBaseLogin(user);
                    categoryItems.clear();
                    categoryAdapter.notifyDataSetChanged();

                    hideItem(navigationView, R.id.nav_logout);
                    showItem(navigationView, R.id.nav_sign_up);
                    showItem(navigationView, R.id.nav_login);

                    loggedin = false;
                }else{
                    dataBaseLogin(user);
                    hideItem(navigationView, R.id.nav_login);
                    hideItem(navigationView, R.id.nav_sign_up);
                    showItem(navigationView, R.id.nav_logout);

                }
            }
        };


        mAuth.addAuthStateListener(mAuthListener);


        FirebaseUser user = mAuth.getCurrentUser();
        dataBaseLogin(user);

        categoryList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                 leftEye.setText(((TextView)view.findViewById(R.id.item_left_eye)).getText().toString());
                 rightEye.setText(((TextView)view.findViewById(R.id.item_right_eye)).getText().toString());
                textChangeListener();
            }
        });
    }



    public void changeName(){
            Log.d("user", nameS + " " + emailS);
            nameT = findViewById(R.id.username);
            emailT = findViewById(R.id.userid);

            //Log.d("TESt", nameT.getText().toString() + emailT.getText().toString());
            nameT.setText(nameS);
            emailT.setText(emailS);


    }

    public void firebaseAuthSync(){

        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();
        mAuth = FirebaseAuth.getInstance();

        // Read from the database
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                if(mAuth.getCurrentUser() != null) {
                    Iterable<DataSnapshot> value = dataSnapshot.child(mAuth.getCurrentUser().getUid()).child("category").getChildren();
                    categoryItems.clear();
                    if (!value.iterator().hasNext()) {
                        categoryAdapter.notifyDataSetChanged();
                    }
                    while (value.iterator().hasNext()) {
                        DataSnapshot ds = value.iterator().next();
                        CategoryItem ci = ds.getValue(CategoryItem.class);
                        categoryItems.add(new CategoryItem(ci.leftEye, ci.rightEye, ds.getKey()));
                        categoryAdapter.notifyDataSetChanged();

                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });
    }



    @Override
    public void onStart() {
        super.onStart();
    }

    public void dataBaseLogin(FirebaseUser user){
        // Check if user is signed in (non-null) and update UI accordingly.

        if(user != null){
            firebaseAuthSync();
            DatabaseReference refname = FirebaseDatabase.getInstance().getReference();
            refname.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot snapshot) {
                    nameS = snapshot.child("user").child(mAuth.getCurrentUser().getUid()).child("nameS").getValue(String.class);
                    emailS = snapshot.child("user").child(mAuth.getCurrentUser().getUid()).child("emailS").getValue(String.class);


                    Log.d("user", nameS + " " + emailS);

                }
                @Override
                public void onCancelled(DatabaseError firebaseError) {
                    Log.e("Read failed", firebaseError.getMessage());
                }
            });

            loggedin = true;
        }else {
            loggedin = false;
        }
    }

    public void databaseAddItem(String name, String left, String right){

        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();

        CategoryItem item = new CategoryItem(left, right);

        databaseReference.child(mAuth.getCurrentUser().getUid()).child("category").child(name).setValue(item);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.action_settings:

                mBluetoothDialog = new BluetoothDialog();
                mBluetoothDialog.show(getFragmentManager(), "Bluetooth Scan");

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public void onClick(View view) {
        changeName();
        switch(view.getId()){
            case R.id.add_category_btn:
                if(loggedin) {
                    mCategoryDialog = new CategoryDialog();
                    mCategoryDialog.show(getFragmentManager(), "Write a name to the category");
                }else{
                    Toast.makeText(this,
                            "You must Log In in order to save your categories",
                            Toast.LENGTH_LONG).show();
                }
                break;

            case R.id.left_plus_btn:
                operOnEye(leftEye, true);
                break;
            case R.id.right_plus_btn:
                operOnEye(rightEye, true);
                break;
            case R.id.left_minus_btn:
                operOnEye(leftEye, false);
                break;
            case R.id.right_minus_btn:
                operOnEye(rightEye, false);
                break;

            default:
        }
    }


    public void operOnEye(TextView tv, boolean oper){
        double num = Double.parseDouble((String)tv.getText());

        if(oper){
            if(num <= 5.5){
                num+=0.5;
            }
        }else{
            if(num >= -5.5){
                num-=0.5;
            }
        }

        tv.setText(num+"");
        textChangeListener();

    }


    public void textChangeListener(){
        String left = leftEye.getText().toString(),
                right = rightEye.getText().toString();

        int leftN = (int) (Float.parseFloat(left)*10);
        int rightN = (int) (Float.parseFloat(right)*10);

        //BluetoothGattService notificationService  = notificationService.getCharacteristic(GattService.CHAR_UUID);

        if(currentGatt  != null) {
            services = currentGatt.getServices();
            BluetoothGattService notificationService = currentGatt.getService(GattService.SERVICE_UUID);
            if (notificationService != null) {

                BluetoothGattCharacteristic characteristic = notificationService.getCharacteristic(GattService.CHAR_UUID);

                if (characteristic == null) {
                    Log.w(TAG, "Send characteristic not found");
                }
                byte[] data = (convertedSet(leftN, rightN)).getBytes();
                characteristic.setValue(data);


                characteristic.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE);
                currentGatt.writeCharacteristic(characteristic);
            } else {
                Toast.makeText(this, "Can't connect to device", Toast.LENGTH_SHORT).show();
            }
        }
    }


    @Override
    public void onCategoryName(String name) {

        String categoryName = name;
        String categoryLeftEye = (String) leftEye.getText();
        String categoryRightEye =  (String)rightEye.getText();

        databaseAddItem(categoryName, categoryLeftEye, categoryRightEye);
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.nav_login:
                startActivity(new Intent(this, LoginActivity.class));
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                return true;
            case R.id.nav_sign_up:
                    startActivity(new Intent(this, SignUpActivity.class));
                    overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                return true;

            case R.id.nav_logout:
                FirebaseAuth.getInstance().signOut();
                nameS = "No Account";
                emailS = "";
                changeName();
        }
        return false;
    }

    private void hideItem(NavigationView nv, int id){
        Menu nav_Menu = nv.getMenu();
        nav_Menu.findItem(id).setVisible(false);
    }
    private void showItem(NavigationView nv, int id){
        Menu nav_Menu = nv.getMenu();
        nav_Menu.findItem(id).setVisible(true);
    }

    public String convertedSet(int left, int right){

        left/=5;
        right/=5;

        left+=12;
        right+=12;
        return Character.toString((char)left)+Character.toString((char)right);
    }




    /*


            Bluetooth Services



     */

    public static BluetoothGattCallback gattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            super.onConnectionStateChange(gatt, status, newState);
            if(newState == BluetoothProfile.STATE_CONNECTED) {
                Log.d("BLE", "BLE Connected!");
                currentGatt.discoverServices();
            }else{
                Log.d("BLE", "BLE is not Connected!");
            }
        }

        @Override
        public void onServicesDiscovered(final BluetoothGatt gatt, int status) {
            super.onServicesDiscovered(gatt, status);
            services = currentGatt.getServices();
            for(BluetoothGattService service : services){
               if(service.getUuid() == GattService.CHAR_UUID){
                   currentGatt.readCharacteristic(service.getCharacteristics().get(0));
               }
            }
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicRead(gatt, characteristic, status);
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicWrite(gatt, characteristic, status);
            gatt.executeReliableWrite();

        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            super.onCharacteristicChanged(gatt, characteristic);
        }

        @Override
        public void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            super.onDescriptorRead(gatt, descriptor, status);

        }

        @Override
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            super.onDescriptorWrite(gatt, descriptor, status);
        }

        @Override
        public void onReliableWriteCompleted(BluetoothGatt gatt, int status) {
            super.onReliableWriteCompleted(gatt, status);
        }

        @Override
        public void onReadRemoteRssi(BluetoothGatt gatt, int rssi, int status) {
            super.onReadRemoteRssi(gatt, rssi, status);
        }

        @Override
        public void onMtuChanged(BluetoothGatt gatt, int mtu, int status) {
            super.onMtuChanged(gatt, mtu, status);
        }
    };
}
