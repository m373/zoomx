package com.zoomx.poocoder.zoomx;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by poocoder on 11/14/17.
 */

@IgnoreExtraProperties
public class CategoryItem {
    String leftEye, rightEye, name;

    public CategoryItem(String leftEye, String rightEye){
        this.leftEye = leftEye;
        this.rightEye = rightEye;
    }

    public String getName() {
        return name;
    }

    public CategoryItem(String leftEye, String rightEye, String name){
        this.leftEye = leftEye;
        this.rightEye = rightEye;
        this.name = name;

    }

    public CategoryItem(){

    }

    public String getLeftEye() {
        return leftEye;
    }

    public String getRightEye() {
        return rightEye;
    }

    public void setRightEye(String rightEye) {
        this.rightEye = rightEye;
    }

    public void setLeftEye(String leftEye) {

        this.leftEye = leftEye;
    }
}
