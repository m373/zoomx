package com.zoomx.poocoder.zoomx;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.app.DialogFragment;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by poocoder on 11/14/17.
 */

public class CategoryDialog extends DialogFragment{

    EditText categoryName;
    Button addButton;

    public interface OnCategoryNameListener{
        public abstract void onCategoryName(String name);
    }

    private OnCategoryNameListener categoryNameListener;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater layoutInflater = getActivity().getLayoutInflater();
        final View v;
        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity())
                .setView(v = layoutInflater.inflate(R.layout.name_category_dialog, null))
                .setTitle(R.string.write_a_name)
                .setPositiveButton("OK",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        EditText catName = v.findViewById(R.id.category_name_edit);
                        String catNameS = catName.getText().toString();
                        addButton = v.findViewById(R.id.add_category_btn);
                        if(!catNameS.equals("")) {
                            categoryName = v.findViewById(R.id.category_name_edit);
                            categoryNameListener.onCategoryName(categoryName.getText().toString());
                        }else{
                            Toast.makeText(getActivity(), "Category name can't be empty",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });


        return adb.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try{
            this.categoryNameListener = (OnCategoryNameListener) context;
        }
        catch (final ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement OnCompleteListener");
        }

    }
}
